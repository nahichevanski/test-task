package server

import (
	"context"
	"errors"
	"lbs/internal/config"
	"lbs/internal/model"
	"net/http"

	_ "lbs/docs"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	httpSwagger "github.com/swaggo/http-swagger/v2"
)

type Service interface {
	GetUser(ctx context.Context, userID int) (model.User, error)
	GetUsers(ctx context.Context) ([]model.User, error)
	PostUser(ctx context.Context, fullName, email, password string) (int, error)
	PutUser(ctx context.Context, fullName, email, password string, userID int) error
	DeleteUser(ctx context.Context, userID int) error
	PostObject(ctx context.Context, userID int, address string) (int, error)
	DeleteObject(ctx context.Context, objectID int) error
	GetObjectsByUserId(ctx context.Context, userID int) ([]model.Object, error)
	PostContract(ctx context.Context, objectID int) (int, error)
	PutContract(ctx context.Context, contractID, objectID int, action string) error
	GetContractsByObjectID(ctx context.Context, objectID int) ([]model.Contract, error)
}

type Server struct {
	router  *chi.Mux
	service Service
	httpSrv *http.Server
}

func New(s Service) (*Server, error) {
	if s == nil {
		return nil, errors.New("server is nil")
	}
	return &Server{
		router:  chi.NewRouter(),
		service: s,
	}, nil
}

// @title тестовое задание
// @version 0.0.1.
// @server http://localhost:24680/ test server
func (srv *Server) Run(cfg *config.Config) error {

	srv.router.Use(middleware.Logger)
	srv.router.Use(middleware.Recoverer)

	srv.router.Route("/users", func(r chi.Router) {
		r.Get("/{id}", srv.User)
		r.Get("/", srv.Users)
		r.Post("/", srv.CreateUser)
		r.Put("/{id}", srv.UpdateUser)
		r.Delete("/{id}", srv.RemoveUser)
	})

	srv.router.Route("/objects", func(r chi.Router) {
		r.Post("/", srv.CreateObject)
		r.Delete("/{id}", srv.RemoveObject)
		r.Get("/{userID}", srv.ObjectsByUserId)
	})

	srv.router.Route("/contracts", func(r chi.Router) {
		r.Post("/", srv.CreateContract)
		r.Put("/", srv.UpdateContract)
		r.Get("/{objectID}", srv.ContractsByObjectID)
	})

	srv.router.Get("/swagger/*", httpSwagger.Handler(
		httpSwagger.URL("http://localhost:24680/swagger/doc.json"),
	))

	srv.httpSrv = &http.Server{
		Addr:    cfg.Server.Addr,
		Handler: srv.router,
	}

	err := srv.httpSrv.ListenAndServe()
	if err != nil {
		return errors.New("server didn't start listen and serve in srv.Run")
	}
	return nil
}

func (srv *Server) Shutdown(ctx context.Context) error {
	if srv.httpSrv != nil {
		return srv.httpSrv.Shutdown(ctx)
	}
	return nil
}
