package server

import (
	"encoding/json"
	"errors"
	"lbs/internal/dto"
	"lbs/internal/storage"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"
)

// @Summary получение одного юзера по id
// @Tags Users
// @Id getUserById
// @Param id path string true "id юзера"
// @Produce json
// @Success 200 {object} dto.User
// @Failure 400 {string} BAD_ID_ERROR, USER_NOT_FOUND_ERROR
// @Failure 500 {string} SERVER_ERROR
// @Router /users/{id} [get]
func (srv *Server) User(rw http.ResponseWriter, req *http.Request) {
	const oper = "server.User"

	id := chi.URLParam(req, "id")

	userID, err := strconv.Atoi(id)
	if err != nil {
		http.Error(rw, "BAD_ID_ERROR", http.StatusBadRequest)
		return
	}

	user, err := srv.service.GetUser(req.Context(), userID)
	if err != nil {
		if errors.Is(err, storage.ErrUsersNotFound) {
			http.Error(rw, "USER_NOT_FOUND_ERROR", http.StatusBadRequest)
			return
		}
		http.Error(rw, "SERVER_ERROR", http.StatusInternalServerError)
		return
	}

	dtoUser := dto.User{
		ID:       user.ID,
		FullName: user.FullName,
		Email:    user.Email,
	}

	userBytes, err := json.Marshal(dtoUser)
	if err != nil {
		http.Error(rw, "SERVER_ERROR", http.StatusInternalServerError)
		return
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write(userBytes)
}

// @Summary получение списка всех пользователей
// @Tags Users
// @Id getAllUsers
// @Produce json
// @Success 200 {array} dto.User
// @Failure 400 {string} USERS_NOT_FOUND_ERROR
// @Failure 500 {string} SERVER_ERROR
// @Router /users [get]
func (srv *Server) Users(rw http.ResponseWriter, req *http.Request) {
	const oper = "server.Users"

	users, err := srv.service.GetUsers(req.Context())
	if err != nil {
		if errors.Is(err, storage.ErrUsersNotFound) {
			http.Error(rw, "USERS_NOT_FOUND_ERROR", http.StatusBadRequest)
			return
		}
		http.Error(rw, "SERVER_ERROR", http.StatusInternalServerError)
		return
	}

	dtoUsers := make([]dto.User, 0, 8)
	for _, user := range users {

		dtoUser := dto.User{
			FullName: user.FullName,
			Email:    user.Email,
			ID:       user.ID,
		}

		dtoUsers = append(dtoUsers, dtoUser)
	}

	usersBytes, err := json.Marshal(dtoUsers)
	if err != nil {
		http.Error(rw, "SERVER_ERROR", http.StatusInternalServerError)
		return
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write(usersBytes)
}

// @Summary создание одного юзера
// @Tags Users
// @Id createUser
// @Accept json
// @Produce json
// @Param fullname body string true "Full Name"
// @Param email body string true "Email"
// @Param password body string true "Password"
// @Success 200 {string} string
// @Failure 400 {string} INVALID_JSON_ERROR, EMPTY_NAME_ERROR, EMPTY_EMAIL_ERROR, EMPTY_PASSWORD_ERROR, USER_ALREADY_EXISTS_ERROR
// @Failure 500 {string} SERVER_ERROR
// @Router /users [post]
func (srv *Server) CreateUser(rw http.ResponseWriter, req *http.Request) {

	var requestData map[string]string
	err := json.NewDecoder(req.Body).Decode(&requestData)
	if err != nil {
		http.Error(rw, "INVALID_JSON_ERROR", http.StatusBadRequest)
		return
	}

	fullName := requestData["fullname"]
	if fullName == "" {
		http.Error(rw, "EMPTY_NAME_ERROR", http.StatusBadRequest)
		return
	}

	email := requestData["email"]
	if email == "" {
		http.Error(rw, "EMPTY_EMAIL_ERROR", http.StatusBadRequest)
		return
	}

	password := requestData["password"]
	if password == "" {
		http.Error(rw, "EMPTY_PASSWORD_ERROR", http.StatusBadRequest)
		return
	}

	userID, err := srv.service.PostUser(req.Context(), fullName, email, password)
	if err != nil {
		if errors.Is(err, storage.ErrUsersAlreadyExist) {
			http.Error(rw, "USER_ALREADY_EXISTS_ERROR", http.StatusBadRequest)
			return
		}

		http.Error(rw, "SERVER_ERROR", http.StatusInternalServerError)
		return
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write([]byte(strconv.Itoa(userID)))
}

// @Summary обновление пользователя по id
// @Tags Users
// @Id updateUserById
// @Param id path string true "id юзера"
// @Accept json
// @Produce json
// @Param fullname body string true "Full Name"
// @Param email body string true "Email"
// @Param password body string true "Password"
// @Success 200 {string} string
// @Failure 400 {string} BAD_ID_ERROR, INVALID_JSON_ERROR, EMPTY_NAME_ERROR, EMPTY_EMAIL_ERROR, EMPTY_PASSWORD_ERROR, USER_NOT_FOUND_ERROR
// @Failure 500 {string} SERVER_ERROR
// @Router /users/{id} [put]
func (srv *Server) UpdateUser(rw http.ResponseWriter, req *http.Request) {

	userID, err := strconv.Atoi(chi.URLParam(req, "id"))
	if err != nil {
		http.Error(rw, "BAD_ID_ERROR", http.StatusBadRequest)
		return
	}

	var requestData map[string]string
	err = json.NewDecoder(req.Body).Decode(&requestData)
	if err != nil {
		http.Error(rw, "INVALID_JSON_ERROR", http.StatusBadRequest)
		return
	}

	fullName := requestData["fullname"]
	if fullName == "" {
		http.Error(rw, "EMPTY_NAME_ERROR", http.StatusBadRequest)
		return
	}

	email := requestData["email"]
	if email == "" {
		http.Error(rw, "EMPTY_EMAIL_ERROR", http.StatusBadRequest)
		return
	}

	password := requestData["password"]
	if password == "" {
		http.Error(rw, "EMPTY_PASSWORD_ERROR", http.StatusBadRequest)
		return
	}

	err = srv.service.PutUser(req.Context(), fullName, email, password, userID)
	if err != nil {
		if errors.Is(err, storage.ErrUsersNotFound) {
			http.Error(rw, "USER_NOT_FOUND_ERROR", http.StatusBadRequest)
			return
		}

		http.Error(rw, "SERVER_ERROR", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
}

// @Summary удаление пользователя по id
// @Tags Users
// @Id deleteUserById
// @Param id path string true "id юзера"
// @Success 200 {string} string
// @Failure 400 {string} BAD_ID_ERROR, USER_NOT_FOUND_ERROR
// @Failure 500 {string} SERVER_ERROR
// @Router /users/{id} [delete]
func (srv *Server) RemoveUser(rw http.ResponseWriter, req *http.Request) {
	id := chi.URLParam(req, "id")

	userID, err := strconv.Atoi(id)
	if err != nil {
		http.Error(rw, "BAD_ID_ERROR", http.StatusBadRequest)
		return
	}

	err = srv.service.DeleteUser(req.Context(), userID)
	if err != nil {
		if errors.Is(err, storage.ErrUsersNotFound) {
			http.Error(rw, "USER_NOT_FOUND_ERROR", http.StatusBadRequest)
			return
		}
		http.Error(rw, "SERVER_ERROR", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
}

// "POST/objects"
func (srv *Server) CreateObject(rw http.ResponseWriter, req *http.Request) {

	var requestData map[string]string
	err := json.NewDecoder(req.Body).Decode(&requestData)
	if err != nil {
		http.Error(rw, "INVALID_JSON_ERROR", http.StatusBadRequest)
		return
	}

	userID, err := strconv.Atoi(requestData["user_id"])
	if err != nil {
		http.Error(rw, "BAD_ID_ERROR", http.StatusBadRequest)
		return
	}

	address := requestData["address"]
	if address == "" {
		http.Error(rw, "EMPTY_ADDRESS_ERROR", http.StatusBadRequest)
		return
	}

	objectID, err := srv.service.PostObject(req.Context(), userID, address)
	if err != nil {
		if errors.Is(err, storage.ErrObjectAlreadyExists) {
			http.Error(rw, "OBJECT_ALREADY_EXISTS_ERROR", http.StatusBadRequest)
			return
		}
		http.Error(rw, "SERVER_ERROR", http.StatusInternalServerError)
		return
	}
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write([]byte(strconv.Itoa(objectID)))
}

// "DELETE/objects/{id}"
func (srv *Server) RemoveObject(rw http.ResponseWriter, req *http.Request) {
	id := chi.URLParam(req, "id")

	objectID, err := strconv.Atoi(id)
	if err != nil {
		http.Error(rw, "BAD_ID_ERROR", http.StatusBadRequest)
		return
	}

	err = srv.service.DeleteObject(req.Context(), objectID)
	if err != nil {
		if errors.Is(err, storage.ErrObjectNotFound) {
			http.Error(rw, "OBJECT_NOT_FOUND_ERROR", http.StatusBadRequest)
			return
		}
		http.Error(rw, "SERVER_ERROR", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
}

// GET/objects/{userID}
func (srv *Server) ObjectsByUserId(rw http.ResponseWriter, req *http.Request) {
	userID, err := strconv.Atoi(chi.URLParam(req, "userID"))
	if err != nil {
		http.Error(rw, "BAD_ID_ERROR", http.StatusBadRequest)
		return
	}

	objects, err := srv.service.GetObjectsByUserId(req.Context(), userID)
	if err != nil {
		if errors.Is(err, storage.ErrObjectNotFound) {
			http.Error(rw, "OBJECTS_NOT_FOUND_ERROR", http.StatusBadRequest)
			return
		}
		http.Error(rw, "SERVER_ERROR", http.StatusInternalServerError)
		return
	}

	dtoObjects := make([]dto.Object, 0, 8)

	for _, v := range objects {

		dtoObject := dto.Object{
			Address:   v.Address,
			IsVisible: v.IsVisible,
		}

		dtoObjects = append(dtoObjects, dtoObject)
	}

	objectsBytes, err := json.Marshal(dtoObjects)
	if err != nil {
		http.Error(rw, "SERVER_ERROR", http.StatusInternalServerError)
		return
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write(objectsBytes)
}

// "POST/contracts"
func (srv *Server) CreateContract(rw http.ResponseWriter, req *http.Request) {
	objectID, err := strconv.Atoi(req.FormValue("objectID"))
	if err != nil {
		http.Error(rw, "BAD_ID_ERROR", http.StatusBadRequest)
		return
	}

	contractID, err := srv.service.PostContract(req.Context(), objectID)
	if err != nil {
		if errors.Is(err, storage.ErrContractAlreadyExist) {
			http.Error(rw, "", http.StatusBadRequest)
			return
		}
		http.Error(rw, "SERVER_ERROR", http.StatusInternalServerError)
		return
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusCreated)
	rw.Write([]byte(strconv.Itoa(contractID)))
}

// "PUT/contracts/"
func (srv *Server) UpdateContract(rw http.ResponseWriter, req *http.Request) {
	contractID, err := strconv.Atoi(req.FormValue("contractID"))
	if err != nil {
		http.Error(rw, "BAD_CONTRACT_ID_ERROR", http.StatusBadRequest)
		return
	}

	objectID, err := strconv.Atoi(req.FormValue("objectID"))
	if err != nil {
		http.Error(rw, "BAD_OBJECT_ID_ERROR", http.StatusBadRequest)
		return
	}

	action := req.FormValue("action")
	if action == "" {
		http.Error(rw, "EMPTY_ACTION_PARAMETER_ERROR", http.StatusBadRequest)
		return
	}

	err = srv.service.PutContract(req.Context(), contractID, objectID, action)
	if err != nil {
		http.Error(rw, "SERVER_ERROR", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
}

// "GET/contracts/{objectID}"
func (srv *Server) ContractsByObjectID(rw http.ResponseWriter, req *http.Request) {
	objectID, err := strconv.Atoi(chi.URLParam(req, "objectID"))
	if err != nil {
		http.Error(rw, "BAD_ID_ERROR", http.StatusBadRequest)
		return
	}

	contracts, err := srv.service.GetContractsByObjectID(req.Context(), objectID)
	if err != nil {
		if errors.Is(err, storage.ErrContractNotFound) {
			http.Error(rw, "CONTRACT_NOT_FOUND_ERROR", http.StatusBadRequest)
			return
		}
		http.Error(rw, "SERVER_ERROR", http.StatusInternalServerError)
		return
	}

	dtoContracts := make([]dto.Contract, 0, 8)

	for _, v := range contracts {

		dtoContract := dto.Contract{
			Number: v.Number,
			Date:   v.Date,
			Status: v.Status,
		}

		dtoContracts = append(dtoContracts, dtoContract)
	}

	contractBytes, err := json.Marshal(dtoContracts)
	if err != nil {
		http.Error(rw, "SERVER_ERROR", http.StatusInternalServerError)
		return
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write(contractBytes)
}
