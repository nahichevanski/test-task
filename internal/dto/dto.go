package dto

import "time"

// @Schema
type User struct {
	FullName string `json:"fullname"`
	Email    string `json:"email"`
	ID       int    `json:"id"`
}

// @Schema
type Contract struct {
	Number string    `json:"number"`
	Date   time.Time `json:"date"`
	Status bool      `json:"status"`
}

// @Schema
type Object struct {
	Address   string `json:"address"`
	IsVisible bool   `json:"is_visible"`
}
