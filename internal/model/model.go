package model

import "time"

type User struct {
	ID       int
	FullName string
	Email    string
	Password string
}

type Object struct {
	Address   string
	ID        int
	UserID    int
	IsVisible bool
}

type Contract struct {
	Number   string
	Date     time.Time
	ID       int
	ObjectID int
	Status   bool
}
