package service

import (
	"context"
	"errors"
	"fmt"
	"lbs/internal/model"
	"lbs/internal/storage"
	"log/slog"
	"strconv"
	"time"
)

type Storage interface {
	User(ctx context.Context, userID int) (model.User, error)
	Users(ctx context.Context) ([]model.User, error)
	CreateUser(ctx context.Context, fullName, email, password string) (int, error)
	UpdateUser(ctx context.Context, fullName, email, password string, userID int) error
	RemoveUser(ctx context.Context, userID int) error
	CreateObject(ctx context.Context, userID int, address string) (int, error)
	RemoveObject(ctx context.Context, objectID int) error
	ObjectsByUserID(ctx context.Context, userID int) ([]model.Object, error)
	CreateContract(ctx context.Context, objectID int, number string) (int, error)
	OpenContract(ctx context.Context, contractID, objectID int) error
	CloseContract(ctx context.Context, contractID, objectID int) error
	ContractsByObjectID(ctx context.Context, objectID int) ([]model.Contract, error)
}

type Service struct {
	log     *slog.Logger
	storage Storage
}

func New(log *slog.Logger, storage Storage) (*Service, error) {

	if log == nil {
		return nil, errors.New("logger is nil")
	}

	if storage == nil {
		return nil, errors.New("storage is nil")
	}

	return &Service{
		log:     log,
		storage: storage,
	}, nil
}

func (s *Service) GetUser(ctx context.Context, userID int) (model.User, error) {
	const oper = "service.GetUser"

	user, err := s.storage.User(ctx, userID)
	if err != nil {
		return model.User{}, fmt.Errorf("%s: %w", oper, err)
	}
	return user, nil
}

func (s *Service) GetUsers(ctx context.Context) ([]model.User, error) {
	const oper = "service.GetUsers"

	users, err := s.storage.Users(ctx)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", oper, err)
	}
	return users, nil
}

func (s *Service) PostUser(ctx context.Context, fullName, email, password string) (int, error) {
	const oper = "service.PostUser"

	userID, err := s.storage.CreateUser(ctx, fullName, email, password)
	if err != nil {
		return 0, fmt.Errorf("%s: %w", oper, err)
	}
	return userID, nil
}

func (s *Service) PutUser(ctx context.Context, fullName, email, password string, userID int) error {
	const oper = "service.PutUser"

	err := s.storage.UpdateUser(ctx, fullName, email, password, userID)
	if err != nil {
		if errors.Is(err, storage.ErrUsersNotFound) {
			return err
		}
		return fmt.Errorf("%s: %w", oper, err)
	}
	return nil
}

func (s *Service) DeleteUser(ctx context.Context, userID int) error {
	const oper = "service.DeleteUser"

	err := s.storage.RemoveUser(ctx, userID)
	if err != nil {
		if errors.Is(err, storage.ErrUsersNotFound) {
			return err
		}
		return fmt.Errorf("%s: %w", oper, err)
	}
	return nil
}

func (s *Service) PostObject(ctx context.Context, userID int, address string) (int, error) {
	const oper = "service.PostObject"

	objectID, err := s.storage.CreateObject(ctx, userID, address)
	if err != nil {
		return 0, fmt.Errorf("%s: %w", oper, err)
	}
	return objectID, nil
}

func (s *Service) DeleteObject(ctx context.Context, objectID int) error {
	const oper = "service.DeleteObject"

	err := s.storage.RemoveObject(ctx, objectID)
	if err != nil {
		return fmt.Errorf("%s: %w", oper, err)
	}
	return nil
}

func (s *Service) GetObjectsByUserId(ctx context.Context, userID int) ([]model.Object, error) {
	const oper = "service.GetObjectsByUserId"

	objects, err := s.storage.ObjectsByUserID(ctx, userID)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	return objects, nil
}

func (s *Service) PostContract(ctx context.Context, objectID int) (int, error) {
	const oper = "service.PostContract"

	number := strconv.FormatInt(time.Now().UnixNano(), 10)

	contractID, err := s.storage.CreateContract(ctx, objectID, number)
	if err != nil {
		return 0, fmt.Errorf("%s: %w", oper, err)
	}
	return contractID, nil
}

func (s *Service) PutContract(ctx context.Context, contractID, objectID int, action string) error {
	const oper = "service.PutContract"

	if action == "open" {

		err := s.storage.OpenContract(ctx, contractID, objectID)
		if err != nil {
			return fmt.Errorf("%s: %w", oper, err)
		}

	} else if action == "close" {

		err := s.storage.CloseContract(ctx, contractID, objectID)
		if err != nil {
			return fmt.Errorf("%s: %w", oper, err)
		}

	} else {
		return fmt.Errorf("%s: %w", oper, errors.New("action undefined"))
	}

	return nil
}

func (s *Service) GetContractsByObjectID(ctx context.Context, objectID int) ([]model.Contract, error) {
	const oper = "service.GetContractsByObjectID"

	contracts, err := s.storage.ContractsByObjectID(ctx, objectID)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	return contracts, nil
}
