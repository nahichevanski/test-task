package postgres

import (
	"context"
	"fmt"
	"lbs/internal/config"
	"log/slog"
	"os"
	"time"

	"github.com/jackc/pgx/v5/pgxpool"
)

type Storage struct {
	db  *pgxpool.Pool
	log *slog.Logger
}

func New(log *slog.Logger, cfg *config.Config) (*Storage, error) {
	const oper = "postgres.New"

	conn := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable",
		cfg.DB.User, os.Getenv("DB_PASS"), cfg.DB.Host, cfg.DB.Port, cfg.DB.Name)

	connectConfig, err := pgxpool.ParseConfig(conn)
	if err != nil {
		log.Error("Failed to parse connection string")
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	connectConfig.ConnConfig.ConnectTimeout = 5 * time.Second

	var db *pgxpool.Pool
	ctx := context.Background()

	for i := 0; i < 10; i++ {
		db, err = pgxpool.NewWithConfig(ctx, connectConfig)
		if err != nil {
			log.Error(fmt.Sprintf("Failed connection - %d attempts left", 9-i))
			time.Sleep(time.Duration(i) * 100 * time.Millisecond)
		} else {
			log.Debug("Storage created")
			break
		}
	}

	if err != nil {
		log.Error("Storage was not created")
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	err = db.Ping(ctx)
	if err != nil {
		log.Error("failed Ping")
		return nil, fmt.Errorf("%s: %w", oper, err)
	}
	log.Debug("Ping is OK!")

	return &Storage{db: db, log: log}, nil
}

// Stop close database connection
func (s *Storage) Stop() {
	s.db.Close()
}
