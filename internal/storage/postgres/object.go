package postgres

import (
	"context"
	"errors"
	"fmt"
	"lbs/internal/model"
	"lbs/internal/storage"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
)

func (s *Storage) CreateObject(ctx context.Context, userID int, address string) (int, error) {
	const oper = "postgres.CreateObject"

	query := `
		insert into objects(user_id, address)
		values($1, $2)
		returning id;
	`
	var id int
	err := s.db.QueryRow(ctx, query, userID, address).Scan(&id)
	if err != nil {
		pgErr, ok := err.(*pgconn.PgError)
		if ok && pgErr.Code == "23505" {
			return 0, storage.ErrObjectAlreadyExists
		}
		return 0, fmt.Errorf("%s: %w", oper, err)
	}

	return id, nil
}

func (s *Storage) RemoveObject(ctx context.Context, objectID int) error {
	const oper = "postgres.RemoveObject"

	query := `
	DELETE FROM objects
	WHERE id = $1
	AND NOT EXISTS (
		SELECT 1
		FROM contracts
		WHERE contracts.object_id = objects.id
	);
	`
	rows, err := s.db.Query(ctx, query, objectID)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return storage.ErrObjectNotFound
		}
		return fmt.Errorf("%s: %w", oper, err)
	}
	defer rows.Close()

	return nil
}

func (s *Storage) ObjectsByUserID(ctx context.Context, userID int) ([]model.Object, error) {
	const oper = "postgres.ObjectsByUserID"

	query := `
	SELECT * FROM objects
	WHERE user_id = $1; 
	`
	rows, err := s.db.Query(ctx, query, userID)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, storage.ErrObjectNotFound
		}
		return nil, fmt.Errorf("%s: %w", oper, err)
	}
	defer rows.Close()

	objects := make([]model.Object, 0, 8)

	for rows.Next() {
		object := model.Object{}
		err = rows.Scan(&object.Address, &object.ID, &object.UserID, &object.IsVisible)
		if err != nil {
			return nil, fmt.Errorf("%s: %w", oper, err)
		}
		objects = append(objects, object)
	}

	return objects, nil
}
