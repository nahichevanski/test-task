package postgres

import (
	"context"
	"errors"
	"fmt"
	"lbs/internal/model"
	"lbs/internal/storage"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
)

func (s *Storage) Users(ctx context.Context) ([]model.User, error) {
	const oper = "postgres.Users"

	query := `select id, full_name, email, password from users;`

	rows, err := s.db.Query(ctx, query)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, storage.ErrUsersNotFound
		}
		return nil, fmt.Errorf("%s: %w", oper, err)
	}
	defer rows.Close()

	users := make([]model.User, 0, 10)
	for rows.Next() {
		user := model.User{}
		err = rows.Scan(&user.ID, &user.FullName, &user.Email, &user.Password)
		if err != nil {
			return nil, fmt.Errorf("%s: %w", oper, err)
		}
		users = append(users, user)
	}

	return users, nil
}

func (s *Storage) User(ctx context.Context, userID int) (model.User, error) {
	const oper = "postgres.User"

	query := `
		select id, full_name, email, password from users
		where id = $1;`

	row := s.db.QueryRow(ctx, query, userID)

	user := model.User{}

	err := row.Scan(&user.ID, &user.FullName, &user.Email, &user.Password)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return model.User{}, storage.ErrUsersNotFound
		}
		return model.User{}, fmt.Errorf("%s: %w", oper, err)
	}
	return user, nil
}

func (s *Storage) CreateUser(ctx context.Context, fullName, email, password string) (int, error) {
	const oper = "postgres.CreateUser"

	query := `
		insert into users(full_name, email, password)
		values($1, $2, $3)
		returning id;
	`
	var userID int

	err := s.db.QueryRow(ctx, query, fullName, email, password).Scan(&userID)

	if err != nil {
		pgErr, ok := err.(*pgconn.PgError)
		if ok && pgErr.Code == "23505" { // 23505 is the PostgreSQL error code for unique_violation
			return 0, storage.ErrUsersAlreadyExist
		}
		return 0, fmt.Errorf("%s: %w", oper, err)
	}

	// if err != nil {
	// 	var pgErr *pgconn.PgError
	// 	if errors.As(err, &pgErr) {
	// 		if pgErr.Code == "23505" {
	// 			return 0, storage.ErrUsersAlreadyExist
	// 		}
	// 	}
	//  return 0, fmt.Errorf("%s: %w", oper, err)
	// }

	return userID, nil
}

func (s *Storage) UpdateUser(ctx context.Context, fullName, email, password string, userID int) error {
	const oper = "postgres.UpdateUser"

	query := `
		update users
		set full_name = $1, email = $2, password = $3
		where id = $4;
	`

	comTag, err := s.db.Exec(ctx, query, fullName, email, password, userID)
	if err != nil {
		return fmt.Errorf("%s: %w", oper, err)
	}
	if comTag.RowsAffected() == 0 {
		return storage.ErrUsersNotFound
	}
	return nil
}

func (s *Storage) RemoveUser(ctx context.Context, userID int) error {
	const oper = "postgres.RemoveUser"

	query := `
	DELETE FROM users
	WHERE id = $1
	AND NOT EXISTS (
    	SELECT 1
    	FROM objects
    	WHERE objects.user_id = users.id
	);
	`
	var result int
	err := s.db.QueryRow(ctx, query, userID).Scan(&result)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return storage.ErrUsersNotFound
		}
		return fmt.Errorf("%s: %w", oper, err)
	}
	return nil
	/*
		DELETE FROM users
		WHERE id = $1
		AND NOT EXISTS (
			SELECT 1
			FROM objects
			WHERE objects.user_id = users.id
		);

		DELETE FROM users
		WHERE NOT EXISTS (
			SELECT 1 FROM objects WHERE objects.user_id = users.id
		);

		DELETE FROM users
		USING users
		LEFT JOIN objects ON users.id = objects.user_id
		WHERE objects.user_id IS NULL AND users.id = $1;
	*/
}
