package postgres

import (
	"context"
	"errors"
	"fmt"
	"lbs/internal/model"
	"lbs/internal/storage"
	"time"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
)

func (s *Storage) CreateContract(ctx context.Context, objectID int, number string) (int, error) {
	const oper = "postgres.CreateContract"

	date := time.Now().Truncate(time.Hour)

	query := `
	insert into contracts(object_id, date, number)
	values($1, $2, $3)
	returning id
	`
	var id int

	err := s.db.QueryRow(ctx, query, date, objectID, number).Scan(&id)
	if err != nil {
		pgErr, ok := err.(*pgconn.PgError)
		if ok && pgErr.Code == "23505" {
			return 0, storage.ErrContractAlreadyExist
		}
		return 0, fmt.Errorf("%s: %w", oper, err)
	}

	return id, nil
}

func (s *Storage) OpenContract(ctx context.Context, contractID, objectID int) error {
	const oper = "postgres.OpenContract"

	tx, err := s.db.Begin(ctx)
	if err != nil {
		return fmt.Errorf("%s: %w", oper, err)
	}
	defer tx.Rollback(ctx)

	query := `
		UPDATE contracts SET status = TRUE
		WHERE id = $1 AND
			(SELECT COUNT(*) FROM contracts
			WHERE object_id = $2 AND status = TRUE) = 0;
	`
	_, err = tx.Exec(ctx, query, contractID, objectID)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return storage.ErrContractNotFound
		}
		return fmt.Errorf("%s: %w", oper, err)
	}

	query2 := `
		UPDATE objects SET is_visible = TRUE
		WHERE id = $2 AND NOT is_visible = TRUE;
	`
	_, err = tx.Exec(ctx, query2, objectID)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return storage.ErrObjectNotFound
		}
		return fmt.Errorf("%s: %w", oper, err)
	}

	err = tx.Commit(ctx)
	if err != nil {
		return fmt.Errorf("%s: %w", oper, err)
	}

	return nil
}

func (s *Storage) CloseContract(ctx context.Context, contractID, objectID int) error {
	const oper = "postgres.CloseContract"

	tx, err := s.db.Begin(ctx)
	if err != nil {
		return fmt.Errorf("%s: %w", oper, err)
	}
	defer tx.Rollback(ctx)

	query := `
		UPDATE contracts SET status = FALSE
		WHERE id = $1 AND NOT status = FALSE;
	`
	_, err = tx.Exec(ctx, query, contractID)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return storage.ErrContractNotFound
		}
		return fmt.Errorf("%s: %w", oper, err)
	}

	query2 := `
		UPDATE objects SET is_visible = FALSE
		WHERE id = $1 AND NOT is_visible = FALSE;
	`
	_, err = tx.Exec(ctx, query2, objectID)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return storage.ErrObjectNotFound
		}
		return fmt.Errorf("%s: %w", oper, err)
	}
	err = tx.Commit(ctx)
	if err != nil {
		return fmt.Errorf("%s: %w", oper, err)
	}

	return nil
}

func (s *Storage) ContractsByObjectID(ctx context.Context, objectID int) ([]model.Contract, error) {
	const oper = "postgres.ContractsByObjectID"

	query := `
		SELECT * FROM contracts
		WHERE object_id = $1; 
	`
	rows, err := s.db.Query(ctx, query, objectID)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, storage.ErrObjectNotFound
		}
		return nil, fmt.Errorf("%s: %w", oper, err)
	}
	defer rows.Close()

	contracts := make([]model.Contract, 0, 8)

	for rows.Next() {
		contract := model.Contract{}
		err = rows.Scan(&contract.Number, &contract.Date, &contract.ID, &contract.ObjectID, &contract.Status)
		if err != nil {
			return nil, fmt.Errorf("%s: %w", oper, err)
		}
		contracts = append(contracts, contract)
	}

	return contracts, nil
}
