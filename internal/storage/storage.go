package storage

import "errors"

var ErrUsersNotFound = errors.New("users not found")
var ErrUsersAlreadyExist = errors.New("user already exist")
var ErrContractAlreadyExist = errors.New("contract already exist")
var ErrContractNotFound = errors.New("contract not found")
var ErrRelatedContractsExists = errors.New("object has a contract, deletion is prohibited")
var ErrRelatedObjectsExists = errors.New("user has an object, deletion is prohibited")
var ErrObjectNotFound = errors.New("objects not found")
var ErrObjectAlreadyExists = errors.New("object alreafy exist")
