run: ## Start application
run:
	go run cmd/app/main.go

migrate: ## Migrations
migrate:
	go run cmd/migrator/main.go