FROM golang:1.21-alpine AS builder

WORKDIR /test-task

COPY go.mod go.sum ./

RUN apk --no-cache add bash make

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -o ./cmd/app/test ./cmd/app/main.go

FROM alpine AS runner

WORKDIR /test-task

COPY --from=builder test-task/cmd/app/test .
COPY --from=builder test-task/config/config.yaml ./config/ 
COPY --from=builder test-task/.env . 

CMD ["./test"]

EXPOSE 24680