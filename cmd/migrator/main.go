package main

import (
	"errors"
	"fmt"
	"lbs/internal/config"
	"os"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

func main() {

	cfg := config.MustLoad()

	conn := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable",
		cfg.DB.User, os.Getenv("DB_PASS"), cfg.DB.Host, cfg.DB.Port, cfg.DB.Name)

	migPath := "./migrations"

	m, err := migrate.New("file://"+migPath, conn)
	if err != nil {
		panic(err)
	}

	// if err := m.Drop(); err != nil {
	// 	if errors.Is(err, migrate.ErrNoChange) {
	// 		fmt.Println("no migrations to apply")
	// 		return
	// 	}
	// 	panic(err)
	// }

	if err := m.Up(); err != nil {
		if errors.Is(err, migrate.ErrNoChange) {
			fmt.Println("no migrations to apply")
			return
		}
		panic(err)
	}
	fmt.Println("migrations applied")
}
