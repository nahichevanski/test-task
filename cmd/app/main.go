package main

import (
	"context"
	"log/slog"
	"os"
	"os/signal"
	"syscall"

	"lbs/internal/config"
	"lbs/internal/server"
	"lbs/internal/service"
	"lbs/internal/storage/postgres"
)

func main() {

	cfg := config.MustLoad()

	log := slog.New(
		slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}),
	)

	db, err := postgres.New(log, cfg)
	if err != nil {
		log.Error("Database crashed", err)
		os.Exit(1)
	}
	log.Debug("Database connected")

	service, err := service.New(log, db)

	server, err := server.New(service)

	go func() {
		err = server.Run(cfg)
		if err != nil {
			log.Error("Server crashed", err)
		}
	}()

	log.Info("Server started")

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	<-sig

	err = server.Shutdown(context.Background())
	if err != nil {
		log.Error("Server shutting down error")
	}
	log.Info("Server shutting down")

	db.Stop()
	log.Info("Database closed")
}
