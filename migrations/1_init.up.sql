
CREATE TABLE IF NOT EXISTS users (
    id SERIAL PRIMARY KEY,
    full_name VARCHAR(255) NOT NULL,
    email VARCHAR(255) UNIQUE NOT NULL,
    password VARCHAR(255) NOT NULL
);


CREATE TABLE IF NOT EXISTS objects (
    id SERIAL PRIMARY KEY,
    user_id INT REFERENCES users(id),
    address VARCHAR(255) NOT NULL,
    is_visible BOOLEAN DEFAULT false
);


CREATE TABLE IF NOT EXISTS contracts (
    id SERIAL PRIMARY KEY,
    object_id INT REFERENCES objects(id),
    date DATE NOT NULL,
    number VARCHAR(50) UNIQUE NOT NULL,
    status BOOLEAN DEFAULT false
);